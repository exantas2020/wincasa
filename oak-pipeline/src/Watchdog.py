from subprocess import Popen
import logging.config

from LoggerDefinition import logging_config
from dotenv import dotenv_values

logging.config.dictConfig(logging_config)

LOGGER = logging.getLogger("ch.sotaria.watchdog")

config = dotenv_values(".env")
filename = "PipelineManager.py"
while True:
    LOGGER.info("Starting " + filename)
    p = Popen("python " + filename, shell=True)
    p.wait()

