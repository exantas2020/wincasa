import asyncio
import logging.config
import os
import subprocess
import sys
from asyncio import Event
from subprocess import Popen, PIPE

import git
from azure.iot.device import MethodResponse
from azure.iot.device.aio import IoTHubDeviceClient

from pipeline import run_pipeline

from LoggerDefinition import logging_config

logging.config.dictConfig(logging_config)

LOGGER = logging.getLogger("ch.sotaria.pipelinemanager")

pipeline_task = None
ngrokProc = None
stop_event = None
loop = None


def start_ngrok():
    LOGGER.info("%s", os.getcwd())

    return Popen(["nohup", "./ngrok", "tcp", "22"], stdout=PIPE, stderr=PIPE, shell=False)


def restart_manager():
    global ngrokProc
    global pipeline_task
    if ngrokProc:
        ngrokProc.terminate()
    if pipeline_task:
        stop_event.set()
    os.execv(sys.executable, ['python'] + sys.argv)


def update_repo():
    try:
        result = subprocess.run(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE)
        repo_dir = result.stdout.decode('utf-8').strip()
        repo = git.Repo(repo_dir)
        res = repo.git.pull()
        LOGGER.info("%s", res)
        return 200, {"success": True, "stdout": "", "stderr": res}
    except git.GitCommandError as e:
        return 500, {"success": False, "stdout": e.stdout, "stderr": e.stderr}


async def main():
    global pipeline_task
    global stop_event
    global loop
    loop = asyncio.get_running_loop()
    LOGGER.info("Pipeline manager starting!")
    conn_str = os.getenv("CONNECTION_STRING")
    device_client = IoTHubDeviceClient.create_from_connection_string(conn_str)
    await device_client.connect()
    LOGGER.info("connected")
    stop_event = Event()
    pipeline_task = loop.create_task(run_pipeline(device_client, stop_event))

    async def method_request_handler(method_request):
        global pipeline_task
        global ngrokProc
        # Determine how to respond to the method request based on the method name
        if method_request.name == "start_pipeline":
            stop_event.set()
            stop_event.clear()
            pipeline_task = asyncio.get_event_loop().create_task(
                run_pipeline(device_client, stop_event))
            payload = {}
            status = 200
        elif method_request.name == "stop_pipeline":
            stop_event.set()
            payload = {}
            status = 200  # set return status code
            LOGGER.info("executed stop_pipeline")
        elif method_request.name == "update_repo":
            status, payload = update_repo()
        elif method_request.name == "start_ngrok":
            ngrokProc = start_ngrok()
            payload = {}
            status = 200  # set return status code
        elif method_request.name == "restart_manager":
            stop_event.set()
            restart_manager()
            # TODO
            # Test the following
            LOGGER.info("should never see this printed")
            # restarting with ngrok running
            payload = {}
            status = 200
        else:
            payload = {"result": False, "data": "unknown method"}  # set response payload
            status = 400  # set return status code
            LOGGER.info("executed unknown method: %s" + method_request.name)

        # Send the response
        method_response = MethodResponse.create_from_method_request(method_request, status, payload)
        await device_client.send_method_response(method_response)

    # Set the method request handler on the client
    device_client.on_method_request_received = method_request_handler

    await asyncio.Event().wait()
    # Finally, shut down the client
    await device_client.shutdown()


if __name__ == "__main__":
    asyncio.run(main())
    # start_ngrok()
    # print("done")
