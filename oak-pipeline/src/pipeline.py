import asyncio
import json
import logging.config
import os
import uuid
from asyncio import Event
from datetime import datetime
import time
from datetime import date
import blobconverter
import cv2
import depthai as dai
import numpy as np
from azure.iot.device.iothub.models import Message
from dotenv import dotenv_values

from LoggerDefinition import logging_config

logging.config.dictConfig(logging_config)

LOGGER = logging.getLogger("ch.sotaria.pipelinemanager.pipeline")

config = dotenv_values(".env")


# https://github.com/openvinotoolkit/open_model_zoo/tree/04ab99cb9fa8c247ac7fdf58890d0b4246a77113/demos/face_recognition_demo/python

def frame_norm(frame, bbox):
    normVals = np.full(len(bbox), frame.shape[0])
    normVals[::2] = frame.shape[1]
    return (np.clip(np.array(bbox), 0, 1) * normVals).astype(int)

def create_pipeline_tracking(fullFrameTracking=True):
    nnPathDefault = blobconverter.from_zoo(name="person-detection-0202", shaves=6)

    fps = 10

    # Create pipeline
    pipeline = dai.Pipeline()

    # Define sources and outputs
    camRgb = pipeline.create(dai.node.ColorCamera)
    spatialDetectionNetwork = pipeline.create(dai.node.MobileNetSpatialDetectionNetwork)
    monoLeft = pipeline.create(dai.node.MonoCamera)
    monoRight = pipeline.create(dai.node.MonoCamera)
    stereo = pipeline.create(dai.node.StereoDepth)
    objectTracker = pipeline.create(dai.node.ObjectTracker)
    xoutBoundingBoxDepthMapping = pipeline.create(dai.node.XLinkOut)

    xoutRgb = pipeline.create(dai.node.XLinkOut)
    trackerOut = pipeline.create(dai.node.XLinkOut)

    xoutRgb.setStreamName("preview")
    trackerOut.setStreamName("tracklets")
    xoutBoundingBoxDepthMapping.setStreamName("boundingBoxDepthMapping")

    camRgb.setPreviewSize(512, 512)

    camRgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    camRgb.setInterleaved(False)
    camRgb.setColorOrder(dai.ColorCameraProperties.ColorOrder.BGR)

    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_400_P)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    monoRight.setFps(fps)
    monoLeft.setFps(fps)
    camRgb.setFps(fps)

    # setting node configs
    # stereo.initialConfig.setConfidenceThreshold(100)
    # stereo.setSubpixel(True)
    # stereo.setLeftRightCheck(True)
    stereo.setDepthAlign(dai.CameraBoardSocket.RGB)
    stereo.setOutputSize(monoLeft.getResolutionWidth(), monoRight.getResolutionHeight())
    stereo.setDefaultProfilePreset(dai.node.StereoDepth.PresetMode.HIGH_DENSITY)

    # current_config = stereo.initialConfig.get()
    # current_config.algorithmControl.subpixelFractionalBits = 5
    # current_config.postProcessing.median = dai.MedianFilter.MEDIAN_OFF
    # stereo.initialConfig.set(current_config)

    # spatialDetectionNetwork.initialConfig.setResize(512, 512)
    spatialDetectionNetwork.setBlobPath(nnPathDefault)
    spatialDetectionNetwork.setConfidenceThreshold(0.75)
    spatialDetectionNetwork.input.setBlocking(False)
    spatialDetectionNetwork.setBoundingBoxScaleFactor(0.5)
    spatialDetectionNetwork.setDepthLowerThreshold(100)
    spatialDetectionNetwork.setDepthUpperThreshold(15000)

    # objectTracker.setDetectionLabelsToTrack([15])  # track only person
    # possible tracking types: ZERO_TERM_COLOR_HISTOGRAM, ZERO_TERM_IMAGELESS, SHORT_TERM_IMAGELESS, SHORT_TERM_KCF
    objectTracker.setTrackerType(dai.TrackerType.ZERO_TERM_COLOR_HISTOGRAM)
    # take the smallest ID when new object is tracked, possible options: SMALLEST_ID, UNIQUE_ID
    objectTracker.setTrackerIdAssignmentPolicy(dai.TrackerIdAssignmentPolicy.SMALLEST_ID)

    # Linking
    monoLeft.out.link(stereo.left)
    monoRight.out.link(stereo.right)
    spatialDetectionNetwork.boundingBoxMapping.link(xoutBoundingBoxDepthMapping.input)

    camRgb.preview.link(spatialDetectionNetwork.input)
    objectTracker.passthroughTrackerFrame.link(xoutRgb.input)
    objectTracker.out.link(trackerOut.input)

    if fullFrameTracking:
        camRgb.setPreviewKeepAspectRatio(False)
        camRgb.video.link(objectTracker.inputTrackerFrame)
        objectTracker.inputTrackerFrame.setBlocking(False)
        # do not block the pipeline if it's too slow on full frame
        objectTracker.inputTrackerFrame.setQueueSize(2)
    else:
        spatialDetectionNetwork.passthrough.link(objectTracker.inputTrackerFrame)

    spatialDetectionNetwork.passthrough.link(objectTracker.inputDetectionFrame)
    spatialDetectionNetwork.out.link(objectTracker.inputDetections)
    stereo.depth.link(spatialDetectionNetwork.inputDepth)

    xoutDepth = pipeline.create(dai.node.XLinkOut)
    xoutDepth.setStreamName("depth")
    spatialDetectionNetwork.passthroughDepth.link(xoutDepth.input)

    return pipeline


async def run_pipeline(device_client=None, stop_event: Event = Event()):
    while not stop_event.is_set():
        try:
            pipeline = create_pipeline_tracking()
            LOGGER.info("tracking pipeline created")
            with dai.Device(pipeline) as device:
                preview = device.getOutputQueue("preview", 4, False)
                tracklets = device.getOutputQueue("tracklets", 4, False)
                depthQueue = device.getOutputQueue(name="depth", maxSize=4, blocking=False)
                xoutBoundingBoxDepthMapping = device.getOutputQueue(name="boundingBoxDepthMapping", maxSize=4,
                                                                    blocking=False)
                

                # set video codec
                today = date.today()
                now = datetime.now()
                fourcc = cv2.VideoWriter_fourcc(*'XVID')
                out = cv2.VideoWriter('output_{}.avi'.format(now), fourcc, 5.0, (1920, 1080))

                # start timer
                start_time = time.time()

                color = (255, 255, 255)
                IDS = {}

                while not stop_event.is_set():
                    LOGGER.info("looping")
                    imgFrame = preview.get()
                    track = tracklets.get()
                    depth = depthQueue.get()
                    frame = imgFrame.getCvFrame()

                    

                    trackletsData = track.tracklets
                    depthFrame = depth.getFrame()
                    depthFrameColor = cv2.normalize(depthFrame, None, 255, 0, cv2.NORM_INF, cv2.CV_8UC1)
                    depthFrameColor = cv2.equalizeHist(depthFrameColor)
                    depthFrameColor = cv2.applyColorMap(depthFrameColor, cv2.COLORMAP_JET)

                    results = []
                    ts = datetime.utcnow()
                    tstr = ts.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
                    for t in trackletsData:
                        roi = t.roi.denormalize(frame.shape[1], frame.shape[0])
                        x1 = int(roi.topLeft().x)
                        y1 = int(roi.topLeft().y)
                        x2 = int(roi.bottomRight().x)
                        y2 = int(roi.bottomRight().y)
                        confidence = t.srcImgDetection.confidence
                        if t.id in IDS:
                            id = IDS[t.id]
                        else:
                            IDS[t.id] = uuid.uuid4()
                            id = IDS[t.id]
                        results.append({
                            "_2d": f"{x1},{y1},{x2},{y2}",
                            "_3dx": t.spatialCoordinates.x,
                            "_3dy": t.spatialCoordinates.y,
                            "depth": t.spatialCoordinates.z,
                            "_2dx_min": int(x1),
                            "_2dx_max": int(x2),
                            "_2dy_min": int(y1),
                            "_2dy_max": int(y2),
                            "confidence": confidence,
                            "timestamp": tstr,
                            "status": t.status.name,
                            "id": str(id)
                        })

                        if os.getenv("DEBUG"):
                            cv2.putText(frame, f"ID: {[t.id]}", (x1 + 10, y1 + 35), cv2.FONT_HERSHEY_TRIPLEX, 0.5,
                                        255)
                            cv2.putText(frame, t.status.name, (x1 + 10, y1 + 50), cv2.FONT_HERSHEY_TRIPLEX, 0.5,
                                        255)
                            cv2.rectangle(frame, (x1, y1), (x2, y2), color, cv2.FONT_HERSHEY_SIMPLEX)

                            cv2.putText(frame, f"X: {int(t.spatialCoordinates.x)} mm", (x1 + 40, y1 + 105),
                                        cv2.FONT_HERSHEY_TRIPLEX,
                                        1.0, (255, 0, 127))
                            cv2.putText(frame, f"Y: {int(t.spatialCoordinates.y)} mm", (x1 + 40, y1 + 145),
                                        cv2.FONT_HERSHEY_TRIPLEX,
                                        1.0, (255, 0, 127))
                            cv2.putText(frame, f"Z: {int(t.spatialCoordinates.z)} mm", (x1 + 40, y1 + 185),
                                        cv2.FONT_HERSHEY_TRIPLEX,
                                        1.0, (255, 0, 127))
                            cv2.rectangle(depthFrameColor, (x1, y1), (x2, y2), color, cv2.FONT_HERSHEY_SCRIPT_SIMPLEX)

                    if len(trackletsData) != 0:
                        boundingBoxDepthMapping = xoutBoundingBoxDepthMapping.get()
                        ROIS = boundingBoxDepthMapping.getConfigData()
                        for roiData in ROIS:
                            roi = roiData.roi
                            roi = roi.denormalize(depthFrameColor.shape[1], depthFrameColor.shape[0])
                            topLeft = roi.topLeft()
                            bottomRight = roi.bottomRight()
                            xmin = int(topLeft.x)
                            ymin = int(topLeft.y)
                            xmax = int(bottomRight.x)
                            ymax = int(bottomRight.y)

                            cv2.rectangle(depthFrameColor, (xmin, ymin), (xmax, ymax), color, cv2.FONT_HERSHEY_SCRIPT_SIMPLEX)
                            # out.write(frame)


                    if os.getenv("DEBUG"):
                        # imS = cv2.resize(frame, (1920, 1080))
                        # cv2.imshow("tracker", imS)
                        cv2.imshow("tracker", frame)
                        out.write(frame)
                        cv2.imshow("depth", depthFrameColor)
                    else:
                        out.write(frame)
                    if cv2.waitKey(1) == ord('q'):
                        break
                    if results:
                        msg_dict = {
                            "pipeline_type": "TRACKING",
                            "results": results
                        }
                        if os.getenv("DEBUG"):
                            print(msg_dict)

                        msg = Message(json.dumps(msg_dict))
                        msg.content_encoding = "utf-8"
                        msg.content_type = "application/json"
                        if device_client is not None:
                            await device_client.send_message(msg)
            # release resources
            out.release()
            cv2.destroyAllWindows()
        except Exception as e:
            LOGGER.error("Pipeline crashed!")
            LOGGER.error(e)


if __name__ == '__main__':
    asyncio.run(run_pipeline())
