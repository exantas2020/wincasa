import yaml
import os
from dotenv import load_dotenv
import re
load_dotenv()

# Copied from: https://stackoverflow.com/a/52412796/1182207
path_matcher = re.compile(r'\$\{([^}^{]+)\}')


def path_constructor(loader, node):
    """ Extract the matched value, expand env variable, and replace the match """
    value = node.value
    match = path_matcher.match(value)
    env_var = match.group()[2:-1]
    return os.environ.get(env_var, 'DUMMY') + value[match.end():]


yaml.add_implicit_resolver('!path', path_matcher)
yaml.add_constructor('!path', path_constructor)
data = """
env: > 
    ${ID}/file.txt
other: file.txt
"""
with open('./logging.yaml', 'rt') as f:
    read = f.read()
    logging_config = yaml.load(read, yaml.FullLoader)
    # logging_config["formatters"]["simple"]["format"] = logging_config["formatters"]["simple"]["format"]


print("logging config", logging_config)
