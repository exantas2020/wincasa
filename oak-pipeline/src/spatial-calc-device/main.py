#!/usr/bin/env python3

import cv2
import depthai as dai
from utility import *
import numpy as np
import math

# Create pipeline
pipeline = dai.Pipeline()

# Define sources and outputs
monoLeft = pipeline.create(dai.node.MonoCamera)
monoRight = pipeline.create(dai.node.MonoCamera)
rgb = pipeline.create(dai.node.ColorCamera)
# Properties
monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_800_P)
monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_800_P)
monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
rgb.setPreviewSize(640, 400)


spatialCalcOut = pipeline.createXLinkOut()
spatialCalcConfigIn = pipeline.createXLinkIn()
spatialCalcDepthIn = pipeline.createXLinkIn()
spatialCalcConfigIn.setStreamName("spatialCalcConfigIn")
spatialCalcDepthIn.setStreamName("spatialCalcDepthIn")
spatialCalcOut.setStreamName("spatialCalcOut")

stereo = pipeline.create(dai.node.StereoDepth)
stereo.setSubpixel(True)
stereo.setLeftRightCheck(True)
stereo.initialConfig.setConfidenceThreshold(100)
stereo.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_3x3)


# Options: MEDIAN_OFF, KERNEL_3x3, KERNEL_5x5, KERNEL_7x7 (default)
"""
depth.initialConfig.setMedianFilter(dai.MedianFilter.KERNEL_7x7)
depth.setLeftRightCheck(lr_check)
depth.setExtendedDisparity(extended_disparity)
depth.setSubpixel(subpixel)
"""
spatialCalc = pipeline.create(dai.node.SpatialLocationCalculator)
spatialCalc.setWaitForConfigInput(True)

xout = pipeline.createXLinkOut()

xout.setStreamName("depth")


spatialCalcConfigIn.out.link(spatialCalc.inputConfig)
spatialCalcDepthIn.out.link(spatialCalc.inputDepth)
spatialCalc.out.link(spatialCalcOut.input)

# Linking
monoLeft.out.link(stereo.left)
monoRight.out.link(stereo.right)
stereo.depth.link(xout.input)


xoutRgb = pipeline.create(dai.node.XLinkOut)
xoutRgb.setStreamName("rgb")
rgb.preview.link(xoutRgb.input)

# Connect to device and start pipeline
with dai.Device(pipeline) as device:
    # Output queue will be used to get the depth frames from the outputs defined above
    depthQueue = device.getOutputQueue(name="depth", maxSize= 4, blocking=False)
    rgbQ = device.getOutputQueue(name="rgb", maxSize= 4, blocking=False)
    spatialCalcConfigInQ = device.getInputQueue("spatialCalcConfigIn")
    spatialCalcDepthInQ = device.getInputQueue("spatialCalcDepthIn")
    spatialCalcOutQ = device.getOutputQueue(name="spatialCalcOut", maxSize=4, blocking=False)

    text = TextHelper()
    y = 200
    x = 300
    step = 3
    delta = 5

    print("Use WASD keys to move ROI.\nUse 'r' and 'f' to change ROI size.")

    while True:
        depthFrame = depthQueue.get()
        rgbFrame = rgbQ.get().getFrame()

        # Calculate spatial coordiantes from depth frame

        spatialCalcConfigData = dai.SpatialLocationCalculatorConfigData()
        spatialCalcConfigData.depthThresholds.lowerThreshold = 100
        spatialCalcConfigData.depthThresholds.upperThreshold = 10000

        topLeft = dai.Point2f(x-delta, y-delta)
        bottomRight = dai.Point2f(x+delta, y+delta)
        spatialCalcConfigData.roi = dai.Rect(topLeft, bottomRight)

        spatialCalcConfigMessage = dai.SpatialLocationCalculatorConfig()
        spatialCalcConfigMessage.addROI(spatialCalcConfigData)

        spatialCalcConfigInQ.send(spatialCalcConfigMessage)
        spatialCalcDepthInQ.send(depthFrame)
        spatials = (spatialCalcOutQ.get().getSpatialLocations()[0]).spatialCoordinates


# Get disparity frame for nicer depth visualization
       
        for f in [rgbFrame]:
            text.rectangle(rgbFrame, (x-delta, y-delta), (x+delta, y+delta))
            text.putText(rgbFrame, "X: " + ("{:.1f}m".format(spatials.x/1000) if not math.isnan(spatials.x) else "--"), (x + 10, y + 20))
            text.putText(rgbFrame, "Y: " + ("{:.1f}m".format(spatials.y/1000) if not math.isnan(spatials.y) else "--"), (x + 10, y + 35))
            text.putText(rgbFrame, "Z: " + ("{:.1f}m".format(spatials.z/1000) if not math.isnan(spatials.z) else "--"), (x + 10, y + 50))



        # Show the frame
        cv2.imshow("depth", rgbFrame)

        key = cv2.waitKey(1)
        if key == ord('q'):
            break
        elif key == ord('w'):
            y -= step
        elif key == ord('a'):
            x -= step
        elif key == ord('s'):
            y += step
        elif key == ord('d'):
            x += step
        elif key == ord('r'): # Increase Delta
            if delta < 50:
                delta += 1
        elif key == ord('f'): # Decrease Delta
            if 3 < delta:
                delta -= 1
