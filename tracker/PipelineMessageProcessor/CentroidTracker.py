import math
import os
from typing import Dict, List, Tuple
import uuid
from rtree import index

# from dotenv import load_dotenv

from .Detection import Detection, Point3D


# load_dotenv()


class CentroidTracker:

    def __init__(self, tracked_objects: Dict[uuid.UUID, list[Detection]] = None):
        if not tracked_objects:
            tracked_objects = {}
        self.uuid_long_map = {}
        self.long_count = 0
        self.new_objects = []
        self.tracked_objects: Dict[uuid.UUID, list[Detection]] = tracked_objects

    def delete(self):
        """ delete the objectID from the tracked interactions """
        self.tracked_objects.clear()

    def register(self, coordinate: Detection, id):
        """ register a new interaction as an objectID """
        self.tracked_objects[id] = [coordinate]

    @staticmethod
    def _get_distance(p1: Point3D, p2: Point3D) -> float:
        x1, y1, z1 = p1
        x2, y2, z2 = p2
        return math.sqrt(math.pow((x1 - x2), 2) + math.pow((y1 - y2), 2) + math.pow((z1 - z2), 2))

    def update(self, detections: list[Detection]) -> Tuple[Dict[uuid.UUID, List[Detection]], List[uuid.UUID]]:
        """ check whether incoming centroids is a new object """
        if not self.tracked_objects:
            for detection in detections:
                id = uuid.uuid4()
                self.new_objects.append(id)
                self.register(detection, id)
        else:
            p = index.Property()
            p.dimension = 3
            tree = index.Index(properties=p)
            for idx, locations in self.tracked_objects.items():
                X, Y, Z = locations[-1].point3d
                self.uuid_long_map[self.long_count] = idx
                tree.insert(self.long_count, (X, Y, Z, X, Y, Z))
                self.long_count += 1

            for detection in detections:
                X, Y, Z = detection.point3d
                nearest_id: int = next(tree.nearest((X, Y, Z, X, Y, Z), objects=False))
                mapped_uuid = self.uuid_long_map[nearest_id]
                nearest_location = self.tracked_objects[mapped_uuid][-1]
                if CentroidTracker.is_close_enough(nearest_location.point3d, detection.point3d):
                    self.tracked_objects[mapped_uuid].append(detection)
                else:
                    id = uuid.uuid4()
                    self.new_objects.append(id)
                    self.register(detection, id)

        return self.tracked_objects.copy(), self.new_objects

    def get_result(self):
        return self.tracked_objects.copy()

    @staticmethod
    def is_close_enough(last_location: Point3D, rect_location: Point3D):
        return CentroidTracker._get_distance(last_location, rect_location) < 2000

        # return CentroidTracker._get_distance(last_location, rect_location) < float(os.getenv("TRACKING_CHANGE_THRESHOLD"))
