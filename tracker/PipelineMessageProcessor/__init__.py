import math
import uuid
from typing import List, Dict, Type
import logging
import json
import azure.functions as func
from .CentroidTracker import CentroidTracker
from .Detection import Detection, Point3D
import pyodbc
import os
import uuid

server = os.environ["DB_HOST"]
database = os.environ["DB_NAME"]
driver = os.environ["DB_DRIVER"]
# driver="{SQL Server Native Client 10.0}"
username = os.environ["DB_USER"]
password = os.environ["DB_PASS"]


# pyodbc.connect(driver=driver,
#                server=server,
#                database=database,
#                uid=username, pwd=password)


def get_device_id(event: func.EventHubEvent):
    return event.metadata["SystemPropertiesArray"][0]["iothub-connection-device-id"]


def get_z_coordinate(x, y, depth):
    return math.sqrt((depth ** 2) - (x ** 2) - (y ** 2))


def get_visit_entry_generator(results, camera_id):
    for d in results:
        _2d = d["_2d"]
        _3dx = d["_3dx"]
        _3dy = d["_3dy"]
        _3dz = d["depth"]
        _2dx_min = d["_2dx_min"]
        _2dx_max = d["_2dx_max"]
        _2dy_min = d["_2dy_min"]
        _2dy_max = d["_2dy_max"]
        confidence = d["confidence"]
        timestamp = d["timestamp"]
        status = d["status"]
        _2d = f"{_2dx_min},{_2dy_min},{_2dx_max},{_2dy_max}"
        _3d = f"{_3dx},{_3dy},{_3dz}"
        # camera_id
        person_id = uuid.UUID(d["id"])
        yield (_2d, _3d, camera_id, person_id, timestamp, _3dx, _3dy, _3dz,
               _2dx_min, _2dx_max, _2dy_min, _2dy_max, confidence, status)


def process_tracking_pipeline_msg(msg, camera_id):
    conn = pyodbc.connect(driver=driver,
                          server=server,
                          database=database,
                          uid=username, pwd=password)

    cursor = conn.cursor()
    cursor.fast_executemany = True
    location_insert_statement = f"""INSERT INTO visit(_2d, _3d, camera_id, person_id, timestamp, _3dx, _3dy, _3dz, 
        _2dxmin, _2dxmax, _2dymin, _2dymax, confidence, status) VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?))"""

    cursor.executemany(location_insert_statement, get_visit_entry_generator(msg["results"], camera_id))
    cursor.commit()
    cursor.close()
    conn.close()



def main(events: List[func.EventHubEvent]):
    # TODO can we gurantee that all events come form the same device ?
    #  No, can't gurantee
    print("Starting...")
    for event in events:
        msg = json.loads(event.get_body().decode('utf-8'))
        iot_hub_id = get_device_id(event)
        device_id = get_camera_id(iot_hub_id)
        if "pipeline_type" in msg and msg.get("pipeline_type") == "TRACKING":
            process_tracking_pipeline_msg(msg, device_id)
            return 
        previous_detections = get_detections(iot_hub_id)
        tracker = CentroidTracker()
        for id, location in previous_detections.items():
            tracker.register(location, id)
        logging.info('Python EventHub trigger processed an event: %s', event.get_body().decode('utf-8'))
        msg = json.loads(event.get_body().decode('utf-8'))

        for d in msg:
            x, y, depth = d.get("_3dx"), d.get("_3dy"), d.get("depth")
            z = get_z_coordinate(x, y, depth)
            d["_3dz"] = z
            d["_3d"] = f"{x},{y},{z}",


        detections = [Detection.from_db_dict(d) for d in msg]
        new_detections, new_person = tracker.update(detections)
        save_new_detections(new_detections, device_id, new_person)
    print("Done!")


def get_camera_id(device_id):
    connection = pyodbc.connect(driver=driver,
                                server=server,
                                database=database,
                                uid=username, pwd=password)
    cur = connection.cursor()
    cur.execute(f"select id from device where iot_hub_id = '{device_id}'")
    res = cur.fetchval()
    cur.close()
    connection.close()
    return res


def get_detections(device_id):
    connection = pyodbc.connect(driver=driver,
                                server=server,
                                database=database,
                                uid=username, pwd=password)
    cur = connection.cursor()
    cur.execute(f"""
    with cte
         AS
         (
             select person_id,
                    camera_id,
                    _3d,
                    _3dx,
                    _3dy,
                    _3dz,
                    timestamp,
                    ROW_NUMBER() OVER (PARTITION BY person_id order by timestamp DESC ) as RN
             From location
         )
SELECT person_id, _3d, timestamp, _3dx, _3dy, _3dz
from cte
inner join device
on camera_id = device.id and iot_hub_id = ?
WHERE RN = 1""", device_id)
    query_res = []
    columns = [column[0] for column in cur.description]
    for row in cur.fetchall():
        query_res.append(dict(zip(columns, row)))
    connection.close()
    res = {}
    print(query_res)
    for d in query_res:
        res[uuid.UUID(d["person_id"])] = Detection.from_db_dict(d)
    return res


def update_detections(device_id, detections):
    connection = pyodbc.connect(driver=driver,
                                server=server,
                                database=database,
                                uid=username, pwd=password)
    cur = connection.cursor()
    cur.execute(f"update device set detections='{detections}'where iot_hub_id='{device_id}'")
    cur.commit()
    connection.close()


def get_location_entry_generator(detections: Dict[Type[uuid.UUID], List[Detection]], camera_id: int, new_person):
    for k, ds in detections.items():
        vs = ds if k in new_person else ds[1:]
        for d in vs:
            _3dx = d.point3d.X
            _3dy = d.point3d.Y
            _3dz = d.point3d.Z
            _2dxmin = d.image_rect.x
            _2dxmax = d.image_rect.w
            _2dymin = d.image_rect.y
            _2dymax = d.image_rect.h
            _2d = f"{_2dxmin},{_2dymin},{_2dxmax},{_2dymax}"
            _3d = f"{_3dx},{_3dy},{_3dz}"
            # camera_id
            person_id = k
            timestamp = d.ts
            confidence = d.confidence
            yield (_2d, _3d, camera_id, person_id, timestamp, _3dx, _3dy, _3dz,
                   _2dxmin, _2dxmax, _2dymin, _2dymax, confidence)


def save_new_detections(new_detections: Dict[uuid.UUID, List[Detection]], camera_id, new_person: List[uuid.UUID]):
    conn = pyodbc.connect(driver=driver,
                          server=server,
                          database=database,
                          uid=username, pwd=password)

    cursor = conn.cursor()
    cursor.fast_executemany = True
    if new_person:
        person_insert_statement = f"INSERT INTO person(id) VALUES (?)"
        cursor.executemany(person_insert_statement, [(p,) for p in new_person])

    location_insert_statement = f"""INSERT INTO location(_2d, _3d, camera_id, person_id, timestamp, _3dx, _3dy, _3dz, 
    _2dxmin, _2dxmax, _2dymin, _2dymax, confidence) VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?))"""

    cursor.executemany(location_insert_statement, get_location_entry_generator(new_detections, camera_id, new_person))
    cursor.commit()
    cursor.close()
    conn.close()
