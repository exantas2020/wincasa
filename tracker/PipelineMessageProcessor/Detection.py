from typing import NamedTuple


class Point3D(NamedTuple):
    X: float
    Y: float
    Z: float


class BoundingRectangle(NamedTuple):
    x: float
    y: float
    w: float
    h: float


class Detection:
    def __init__(self, ts, image_rect: BoundingRectangle, real_world_coordinates: Point3D, confidence: float):
        self.confidence = confidence
        self.ts = ts
        self.image_rect = image_rect
        self.point3d = real_world_coordinates

    @staticmethod
    def from_db_dict(dictionary: dict):
        bb = BoundingRectangle(dictionary.get("_2dxmin"), dictionary.get("_2dymin"), dictionary.get("_2dxmax"), dictionary.get("_2dymax"))
        _3d = Point3D(dictionary.get("_3dx"), dictionary.get("_3dy"), dictionary.get("_3dz"))
        confidence = dictionary.get("confidence")
        return Detection(dictionary.get("timestamp"), bb, _3d, confidence)