


Installing poetry
```
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
```

Add %USERPROFILE%\.poetry\bin to path  
use $profile in powershell to find place of default profile script
$env:Path += ";POETRY_PATH";


Installing poetry on RPI

1) CD into the oak-pipeline and 
2) enter: pi@raspberrypi:~/wincasa/oak-pipeline $ curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
3) enter: poetry
4) python -m venv venv
5) . venv/bin/activate
6) (venv) pi@raspberrypi:~/wincasa/oak-pipeline $ source $HOME/.poetry/env
7) poetry install


** Create .env file to paste IoT Hub Info
1) nano .env
2) CONNECTION_STRING=HostName=wincasaiothubdev.azure-devices.net;DeviceId=TEST_DEVICE;SharedAccessKey=JsmbUYHwtpLYbHzBJ0JfYOmPdCWMtv+dHyatfVmuh6M=
ID=TEST_DEVICE


