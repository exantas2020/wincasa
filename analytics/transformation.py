import pandas as pd
import numpy as np
import geopandas as gpd
import shapely
from shapely.geometry import Point
import re

#  TODO
# Input from DB, per camera
cubes = [
    list(zip([-3461, -3361, -3411, -1964], [8153, 7120, 7088, 6263])),
    list(zip([-709, 569, -1681, -3307], [5404, 6701, 8533, 8370])),
    list(zip([2519, 3061, 1212, 401], [4715, 5342, 6861, 6956]))
]

## General
conf_lvl = 0.6  # select minimum desired confidence level
hour_opening = 7  # select opening /closing hours (temporary to filter sessions)
hour_closing = 18  # see above.
timebucket_type = "d"  # select interval (second, minute, hour, day, week, month, year)
min_person_count = 3

## Areas of interest
numb_cubes = 3

# TODO
# INPUT
df_loc = pd.read_csv("/mnt/c/Users/Mohammad/Downloads/Query.csv")

df_loc['timestamp'] = pd.to_datetime(df_loc['timestamp'])
df_loc['timestamp'] = pd.DatetimeIndex(df_loc['timestamp'])

df_loc['time_hour'] = pd.DatetimeIndex(df_loc['timestamp']).hour
df_loc = df_loc[df_loc['time_hour'].between(hour_opening, hour_closing, "both")]
df_loc = df_loc[df_loc['confidence'] > conf_lvl]
df_loc = df_loc[df_loc['confidence'] > conf_lvl]
df_loc = df_loc[(df_loc["_3dx"] != 0) | (df_loc["_3dy"] != 0) | (df_loc["_3dz"] != 0)]

# gdf = gpd.GeoDataFrame(
#     df_loc, geometry=gpd.points_from_xy(df_loc["_3dx"], df_loc["_3dz"]))

# Creating a shapely Polygon that will be used to "query" the GeoDataFrame

# Creating a new column that indicates which points intersect with
# the query_polygon

col_names = [f'cube{i}_active' for i in range(len(cubes))]
polygons = [shapely.geometry.Polygon(cube) for cube in cubes]


def get_active_cube(row):
    x, z = row["_3dx"], row["_3dz"]
    p = Point(x, z)
    for index, polygon in enumerate(polygons):
        if p.within(polygon):
            return index + 1
    return -1


df_loc["cube"] = df_loc.apply(get_active_cube, axis=1)
# for index, cube in enumerate(cubes):
#     df_loc.apply(  )
#     gdf[f'cube{index + 1}_active'] = gdf.intersects(polygons[index])

df_loc = df_loc.sort_values(["person_id", "timestamp"], ascending=(True, True))

# def filer(row):
#     cube_columns = row[-len(cubes):]
#
#     active_cube_columns = cube_columns[cube_columns == True]
#     # return 0
#     return 0 if len(active_cube_columns) == 0 else re.findall('\d+', active_cube_columns.index.to_list()[0])[0]

# gdf["cube_number"] = gdf.apply(filer, axis=1)

df_loc["cube_shifted"] = df_loc.cube.shift()

interaction_data = df_loc[["person_id", "timestamp", "cube"]].copy()

interaction_data["group"] = np.nan
interaction_data.group.iloc[0] = 1

# interaction_data.ne(interaction_data.shift()).apply(lambda x: x.index[x].tolist())

print("assigning groups")
interaction_data = interaction_data.reset_index(drop=True)
for i in range(1, len(interaction_data)):
    # current_cube = interaction_data["id" == i]["cube"]
    # previous_cube = interaction_data["id" == i - 1]["cube"]
    # previous_group = interaction_data["id" == i - 1]["group"]
    # interaction_data["id" == i]["group"] = previous_group if current_cube == previous_cube else previous_group + 1

    current_cube = interaction_data.iloc[i]["cube"]
    previous_cube = interaction_data.iloc[i - 1]["cube"]
    previous_group = interaction_data.iloc[i - 1]["group"]
    interaction_data.loc[i, "group"] = previous_group if current_cube == previous_cube else previous_group + 1

interaction_data['time_bucket'] = (pd.to_datetime(interaction_data['timestamp'])) \
    .dt.floor(timebucket_type)

interaction_data = interaction_data.groupby(['time_bucket', 'person_id', 'cube', 'group']) \
    .agg(
    dwell_time=('timestamp', np.ptp),
).reset_index()


interaction_data = interaction_data[interaction_data['cube'] != -1]


# Average dwell time, per cube

interaction_data["counter"] = 1
interaction_data = interaction_data.groupby(['time_bucket', 'person_id', 'cube']).agg(
    dwell_sum=('dwell_time', np.sum),  # sum per person per cube, in each time bucket
    dwell_avg=('dwell_time', np.mean), # avg per person per cube, in each time bucket
    interactions=('counter', np.sum), # avg per person per cube, in each time bucket
).reset_index()

# 2 bucket
# 3 people
# 3 cubers

# TOTAL 18 (sum, avg)

interaction_data["counter"] = 1
# interaction_data = interaction_data.drop('group', axis = 1)

interaction_data = interaction_data.groupby(['time_bucket', 'cube']).agg(
    dwell_tot=('dwell_sum', np.sum), # total spent time at the cube
    dwell_av=('dwell_sum', np.mean), # average time spent per cube by a random person across all his interaction
    dwell_av2=('dwell_avg', np.mean), # average time spent per cube by a random person in one interaction
    number=('counter', np.sum), # total people
    interactions=('interactions', np.sum) # interactions >= number
).reset_index()

# 2 bucket
# 3 cubers


input("press any key to continue")
